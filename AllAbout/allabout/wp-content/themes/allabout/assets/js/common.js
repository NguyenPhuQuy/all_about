"use strict"; //Jquery.inview.min.js

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

!function (a) {
  "function" == typeof define && define.amd ? define(["jquery"], a) : "object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) ? module.exports = a(require("jquery")) : a(jQuery);
}(function (a) {
  function i() {
    var b,
        c,
        d = {
      height: f.innerHeight,
      width: f.innerWidth
    };
    return d.height || (b = e.compatMode, (b || !a.support.boxModel) && (c = "CSS1Compat" === b ? g : e.body, d = {
      height: c.clientHeight,
      width: c.clientWidth
    })), d;
  }

  function j() {
    return {
      top: f.pageYOffset || g.scrollTop || e.body.scrollTop,
      left: f.pageXOffset || g.scrollLeft || e.body.scrollLeft
    };
  }

  function k() {
    if (b.length) {
      var e = 0,
          f = a.map(b, function (a) {
        var b = a.data.selector,
            c = a.$element;
        return b ? c.find(b) : c;
      });

      for (c = c || i(), d = d || j(); e < b.length; e++) {
        if (a.contains(g, f[e][0])) {
          var h = a(f[e]),
              k = {
            height: h[0].offsetHeight,
            width: h[0].offsetWidth
          },
              l = h.offset(),
              m = h.data("inview");
          if (!d || !c) return;
          l.top + k.height > d.top && l.top < d.top + c.height && l.left + k.width > d.left && l.left < d.left + c.width ? m || h.data("inview", !0).trigger("inview", [!0]) : m && h.data("inview", !1).trigger("inview", [!1]);
        }
      }
    }
  }

  var c,
      d,
      h,
      b = [],
      e = document,
      f = window,
      g = e.documentElement;
  a.event.special.inview = {
    add: function add(c) {
      b.push({
        data: c,
        $element: a(this),
        element: this
      }), !h && b.length && (h = setInterval(k, 250));
    },
    remove: function remove(a) {
      for (var c = 0; c < b.length; c++) {
        var d = b[c];

        if (d.element === this && d.data.guid === a.guid) {
          b.splice(c, 1);
          break;
        }
      }

      b.length || (clearInterval(h), h = null);
    }
  }, a(f).on("scroll resize scrollstop", function () {
    c = d = null;
  }), !g.addEventListener && g.attachEvent && g.attachEvent("onfocusin", function () {
    d = null;
  });
});

(function () {
  $(document).ready(function () {


    /*===================================================
    Style css element
    ===================================================*/
    $('.slide').css({
      'position': 'fixed',
      'height': '420px',
      'width': '100%'
    });


    /*===================================================
    Slick mainSlide
    ===================================================*/
    $('.slide').slick({
      autoplay: true,
      arrows: true,
      dots: true,
      autoplaySpeed: 2000,
      speed: 1500,
      slidesToShow: 1,
      slidesToScroll: 1,
      centerMode: true,
      variableWidth: true,
      pauseOnFocus: true
    });


    /*===================================================
    Fixed nav
    ===================================================*/
    let _windowNav = $(window);

    let _locationNav = $('.listservice').offset().top;
    $(_windowNav).on('load scroll', function () {
      if (_windowNav.scrollTop() >= _locationNav - 82) {
        $('.listservice').addClass('fixed');
        $('.pickupcontents').addClass('mT420');
      } else {
        $('.listservice').removeClass('fixed');
        $('.pickupcontents').removeClass('mT420');
      }
    });

    
    /*===================================================
    Go to top
    ===================================================*/

    let btnScroll = $('.pageTop');
    $(window).on('load scroll', function (e) {
      e.preventDefault();

      if ($(window).scrollTop()) {
        btnScroll.fadeIn(400);
      } else {
        btnScroll.fadeOut(400);
      }
    });
    btnScroll.on('click', function () {
      $('body,html').animate({
        scrollTop: 0
      }, 1000);
    });

    /*===================================================
    Slick page recruit index
    ===================================================*/
    $('.recruit__wrapper').slick({
      fade: true,
      autoplay: true,
      arrows: false,
      dots: false,
      autoplaySpeed: 2000,
      speed: 1500,
      slidesToShow: 1,
      slidesToScroll: 1,
    });
  
    /*===================================================
    Toggle input text page adifo contactus
    ===================================================*/
    let caution = $('#adinfo__select .caution');
    caution.css('display','none');

    $('#adinfo__select input').on('change', function(){
        $(caution).toggle($(this).val() == '0');
    });


    /*===================================================
    Click scroll page IR
    ===================================================*/
      $(function () { 
        $('.newList3cl li a').click(function () {  
          var target = $(this.hash);  
            target = target.length ? target : $('[name=' + this.hash.substr(1) + ']');  
            if (target.length) {  
            $('html,body').animate({  
            scrollTop: target.offset().top - 130
            }, 1000); 
            return false; 
          } 
        }); 
      });


      /*===================================================
      Scroll Menu hidden bottom or top sevice
      ===================================================*/
      let menuHeight = $('#menu-menu_servicelist').outerHeight(); 
      let headerHeight = $('.header').outerHeight(); 
      let maxSubMenuHeight = $('#menu-menu_servicelist .sub-menu').outerHeight(); 

      $('#menu-menu_servicelist .sub-menu').each(function() {
        if ( $(this).outerHeight() > maxSubMenuHeight ) {
          maxSubMenuHeight = $(this).outerHeight();
        }
      })

      let limitScrollTop = headerHeight + maxSubMenuHeight + 30;
      let currentSpace = $('#menu-menu_servicelist').get(0).getBoundingClientRect().top;

      $(function(){
        if ( currentSpace > limitScrollTop ) {
          $('#menu-menu_servicelist .sub-menu').each(function() {
            $(this).css('bottom', menuHeight + 'px');
          })
        } else {
          $('#menu-menu_servicelist .sub-menu').each(function() {
            $(this).css('bottom', 'auto');
          })
        }

      })

      $(window).scroll(function(){

        currentSpace = $('#menu-menu_servicelist').get(0).getBoundingClientRect().top;

        if ( currentSpace > limitScrollTop ) {
          $('#menu-menu_servicelist .sub-menu').each(function() {
            $(this).css('bottom', menuHeight + 'px');
          })
        } else {
          $('#menu-menu_servicelist .sub-menu').each(function() {
            $(this).css('bottom', 'auto');
          })
        }
      })





  });
})();