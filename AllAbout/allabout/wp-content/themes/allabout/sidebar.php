<aside class="sidebar">

	<?php if (is_tree(49)): ?>
		<?php dynamic_sidebar('sidebar-cor'); ?>
	<?php elseif(is_tree(7)): ?>	
		<?php dynamic_sidebar('sidebar-service'); ?>
		<?php elseif(is_tree(8)): ?>	
		<?php dynamic_sidebar('corporatepress-adinfo'); ?>
	<?php elseif(is_tree(9)): ?>	
		<?php dynamic_sidebar('sidebar-recruit'); ?>
	<?php elseif(is_tree(10)): ?>	
		<?php dynamic_sidebar('sidebar-ir'); ?>	
	<?php elseif(is_tree(11)): ?>	
		<?php dynamic_sidebar('sidebar-guide'); ?>
	<?php elseif(is_tree(12)): ?>	
		<?php dynamic_sidebar('sidebar-adinfo'); ?>	
	<?php elseif(is_tree(483)): ?>	
		<?php dynamic_sidebar('sidebar-inquiry'); ?>					
	<?php endif ?>	
	

	<?php if (is_tree(11)): ?>
		<div class="guideBanner">
	        <a href="https://allabout.co.jp/searchguide/" target="_blank"><img src="<?php  echo THEME_URL ?>/assets/images/guide/guideBanner_01.png" alt="" /></a>
	    </div>
	<?php endif ?>	
	
</aside>