<?php get_header(); ?>
	<!--▼ Main ▼-->
   <main class="main">
      <!-- Listservice -->
      <nav class="listservice subPage">
        <?php allabout_menu('menu_serviceList'); ?>
      </nav>
      <div class="content clearfix">
         <!-- Content -->
         <div class="mainContent">
            <?php 
               _e('<h2>404 NOT FOUND</h2>','allabout');
               _e('<p>The article you were looking for was not found, but maybe try looking again!','allabout');
             ?>
         </div>
      </div>
   </main>
   <!--▲ Main ▲-->
<?php get_footer(); ?>