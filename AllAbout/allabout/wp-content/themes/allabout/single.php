<?php get_header(); ?>
<!--▼ Main ▼-->
   <main class="main">
      <!-- Listservice -->
      <nav class="listservice subPage">
        <?php allabout_menu('menu_serviceList'); ?>
      </nav>
      <div class="content clearfix">
         <!-- Breacrum star -->
         <?php custom_breadcrumbs(); ?>
         <aside class="sidebar">
            <?php get_sidebar(); ?>
         </aside>
         <!-- Content -->
         <div class="mainContent">
            <?php if( have_posts()) : while (have_posts()) : the_post(); ?>
               <?php get_template_part('content', get_post_format()); ?>
            <?php endwhile ?>
            <?php else: ?>
               <?php get_template_part('content', 'none') ?>
            <?php endif; ?>
         </div>
      </div>
   </main>
   <!--▲ Main ▲-->
<?php get_footer(); ?>