<form method="get" role="search" id="searchForm" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <p class="searchTitle">プレスリリース検索</p>
    <input type="hidden" name="search" value="advanced">
    <dl class="searchContent clearfix">
        <dt class="item_business_year">年度</dt>   
        <dd class="item_business_year">
            <select name="business_year" id="postYear">
                <option value=""><?php _e( '選択してください', 'textdomain' ); ?></option>
                <option value="2019"><?php _e( '2019年度', 'textdomain' ); ?></option>
                <option value="2018"><?php _e( '2018年度', 'textdomain' ); ?></option>
                <option value="2017"><?php _e( '2017年度', 'textdomain' ); ?></option>
            </select>
        </dd> 
        <dt class="item_press_genre">カテゴリ</dt>
        <dd class="item_press_genre">
            <SELECT name= press_genre id="postCat">
               <OPTION value="選択してください" selected ><?php _e( '選択してください', 'textdomain' ); ?></OPTION>
               <OPTION value="サービス"><?php _e( 'サービス', 'textdomain' ); ?></OPTION>
               <OPTION value="イベント"><?php _e( 'イベント', 'textdomain' ); ?></OPTION>
               <OPTION value="調査"><?php _e( '調査', 'textdomain' ); ?></OPTION>
               <OPTION value="経営"><?php _e( '経営', 'textdomain' ); ?></OPTION>
               <OPTION value="アワード"><?php _e( 'アワード', 'textdomain' ); ?></OPTION>
            </SELECT>   
        </dd>
        <dt class="item_company_name">会社名</dt>
        <dd class="item_company_name">
            <SELECT name= company_name id="companyName">
               <OPTION value="選択してください" selected ><?php _e( '選択してください', 'textdomain' ); ?></OPTION>
               <OPTION value="オールアバウト"><?php _e( 'オールアバウト', 'textdomain' ); ?></OPTION>
               <OPTION value="オールアバウトナビ"><?php _e( 'オールアバウトナビ', 'textdomain' ); ?></OPTION>
               <OPTION value="オールアバウトライフマーケティング"><?php _e( 'オールアバウトライフマーケティング', 'textdomain' ); ?></OPTION>
               <OPTION value="オールアバウトライフワークス"><?php _e( 'オールアバウトライフワークス', 'textdomain' ); ?></OPTION>
               <OPTION value="生活トレンド研究所"><?php _e( '生活トレンド研究所', 'textdomain' ); ?></OPTION>
               <OPTION value="ファイブスターズゲーム"><?php _e( 'ファイブスターズゲーム', 'textdomain' ); ?></OPTION>
               <OPTION value="ディー・エル・マーケット"><?php _e( 'ディー・エル・マーケット', 'textdomain' ); ?></OPTION>
            </SELECT>   
        </dd>
    </dl>
    <p class="searchSubmit"><input type="submit" id="searchSubmit" value="検索する" /></p>
</form>

