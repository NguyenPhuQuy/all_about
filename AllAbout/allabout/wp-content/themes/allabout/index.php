<?php /* Template Name: Home */ ?>
<?php get_header(); ?>
	<!--▼ Main ▼-->
   <main class="main">
      <!-- Slide -->
      <div class="slide">
         <!-- Item -->
         <?php
         $images = get_field('slide_top');
         if( $images ): ?>
           <?php foreach( $images as $image ): ?>
               <a href="#">
                  <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
               </a>
           <?php endforeach; ?>
         <?php endif; ?>
      </div>
      <!-- Listservice -->
      <nav class="listservice">
         <?php allabout_menu('menu_serviceList'); ?>
      </nav>
      <!-- Pick up Content -->
      <section class="pickupcontents">
         <div class="content">
            <h2>Pickup contents</h2>
            <ul>
               <?php
                  $args_pickup = array(
                     'posts_per_page' => 8,
                     'post_type' => 'pickup',  
                  );
                  $the_query_pickup = new WP_Query( $args_pickup );
                  if ( $the_query_pickup->have_posts() ) :

                  while ( $the_query_pickup->have_posts() ) : $the_query_pickup->the_post();

                  ?>
                     <li>
                        <figure>
                           <?php the_content(); ?>
                           <figcaption>
                              <?php 
                                 $terms_pickup = get_the_terms($the_query->ID, 'pickup_categories');
                                 if ( ! empty( $terms_pickup ) && ! is_wp_error( $terms ) ) {
                                     $count_pickup = count( $terms_pickup );
                                     foreach ( $terms_pickup as $term ) {
                                         echo '<div class="subttl">' . $term->name . '</div>';
                                     }
                                     echo $term_list;
                                 }
                               ?>
                              <h3 class="articlettl" title="<?php echo the_title(); ?>"><?php echo the_title(); ?></h3>
                              <img class="logo" src="<?php the_field('icon') ?>" alt="" />
                           </figcaption>
                        </figure>
                     </li>
                  <?php

                  endwhile;

                  endif;

               ?>
            </ul>
         </div>
      </section>
      <!-- News Post -->
      <div class="news">
         <div class="content">
            <!-- Title -->
            <div class="news__ttl clearfix">
               <h3>プレスリリース</h3>
               <a href="http://localhost/allabout/%e3%83%97%e3%83%ac%e3%82%b9%e3%83%aa%e3%83%aa%e3%83%bc%e3%82%b9/%e6%9c%80%e6%96%b0%e6%83%85%e5%a0%b1%e3%82%92%e3%81%8a%e5%b1%8a%e3%81%91%e3%81%99%e3%82%8b%e3%83%97%e3%83%ac%e3%82%b9%e3%83%aa%e3%83%aa%e3%83%bc%e3%82%b9%e9%85%8d%e4%bf%a1%e5%b8%8c%e6%9c%9b%e3%81%ae/"><span>プレスリリース配信登録</span></a>
            </div>
            <!-- Post item -->
            <ul class="news__lists">
               <?php
                  $args_new = array(
                     'posts_per_page' => 6,
                     'post_status' => 'publish',
                  );
                  $the_query_new = new WP_Query( $args_new );

                  if ( $the_query_new->have_posts() ) :

                  while ( $the_query_new->have_posts() ) : $the_query_new->the_post();

                  ?>
                     <li>
                        <span class="news__date"><?php echo get_the_date('Y年m月d日'); ?></span>
                        <?php
                           $categories_new = get_the_category();
                           if ( ! empty( $categories_new ) ) {
                               echo '<span class="news__cat">' . esc_html( $categories_new[0]->name ) . '</span>';
                           }
                        ?>
                        <span class="news__company"><?php the_field('company'); ?></span> 
                        <div class="news__box">
                           <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                           <span class="news__label"><?php the_field('label'); ?></span>
                        </div>
                     </li>
                  <?php

                  endwhile;

                  endif;

                  wp_reset_postdata();

                  ?>
               </li>
            </ul>
         </div> 
      </div>
      <!-- Banners -->
      <div class="banners">
         <ul class="content">
            <li><a href="http://localhost/allabout/%e5%ba%83%e5%91%8a%e5%95%86%e5%93%81%e3%81%ae%e3%81%94%e6%a1%88%e5%86%85/"><img src="<?php echo THEME_URL ?>/assets/images/index/top_banner_05.png" alt="広告商品のご案内"></a></li>
            <li><a href="https://allabout.co.jp/searchguide/" target="_blank"><img src="<?php echo THEME_URL ?>/assets/images/index/top_banner_02.png" alt="広告商品のご案内"></a></li>
            <li><a href="http://localhost/allabout/%e5%b0%82%e9%96%80%e5%ae%b6%e3%81%ae%e6%96%b9%e3%81%b8/"><img src="<?php echo THEME_URL ?>/assets/images/index/top_banner_04.png" alt="広告商品のご案内"></a></li>
            <li><a href="http://localhost/allabout/%e5%85%ac%e5%bc%8f%e3%82%bd%e3%83%bc%e3%82%b7%e3%83%a3%e3%83%ab%e3%82%a2%e3%82%ab%e3%82%a6%e3%83%b3%e3%83%88/"><img src="<?php echo THEME_URL ?>/assets/images/index/top_banner_01.png" alt="広告商品のご案内"></a></li>
            <li><a href="http://localhost/allabout/%e3%81%8a%e5%95%8f%e5%90%88%e3%81%9b/"><img src="<?php echo THEME_URL ?>/assets/images/index/top_banner_06.png" alt="広告商品のご案内"></a></li>
         </ul>
      </div>   
   </main>
   <!--▲ Main ▲-->
<?php get_footer(); ?>