<?php 

define ('THEME_URL', get_stylesheet_directory_uri());
define('template', THEME_URL . "/template");

if( !function_exists("allabout_theme_setup")) {
    function allabout_theme_setup(){
        /* Setting textdomain */
        $languges_folder = THEME_URL . '/languages';
        load_theme_textdomain('allabout', $languges_folder);

        /* Auto add linl RSS on <head> */
        add_theme_support('automatic-feed-links');

        /* Add post thumnail */ 
        add_theme_support('post-thumbnails');

        /* Post Format */
        add_theme_support('post-formats' ,array(
            'image',
            'video',
            'gallery',
            'quote',
            'link'
        ));

        /* Add title page */
        add_theme_support('title-tag');

        /* Display Title post */
        if( !function_exists('allabout_header')){
            function allabout_header(){?>
                <?php if (is_single()) : ?>
                    <h2><?php the_title(); ?></h2>
                    <?php else : ?>
                        <h2><?php the_title(); ?></h2>
                <?php endif; ?>
           <?php }
        }

        /* Display Title day of Post */
        if( !function_exists('allabout_date')){
            function allabout_date(){?>
                <?php if( !is_page()) : ?>
                    <?php 
                        printf( __('<span class="date"> %1$s','allabout'),
                            get_the_date('Y年m月d日'));

                     ?>
                    <?php endif; ?> 
           <?php }
        }

        /* Add Sidebar for corporate*/
        $sidebar1 = array(
            'name' => __('Corporate Sidebar', 'allabout'),
            'id' => 'sidebar-cor',  
            'deccription' => __('corporate sidebar'),
            'class' => 'sidebar-cor',     
        );
        register_sidebar($sidebar1);

        /* Add Sidebar for service*/
        $sidebar2 = array(
            'name' => __('Service Sidebar', 'allabout'),
            'id' => 'sidebar-service',  
            'deccription' => __('service sidebar'),
            'class' => 'sidebar-service',     
        );
        register_sidebar($sidebar2);

        /* Add Sidebar for recruit*/
        $sidebar3 = array(
            'name' => __('Recruit Sidebar', 'allabout'),
            'id' => 'sidebar-recruit',  
            'deccription' => __('recruit sidebar'),
            'class' => 'sidebar-recruit',     
        );
        register_sidebar($sidebar3);

        /* Add Sidebar for ir*/
        $sidebar4 = array(
            'name' => __('Ir Sidebar', 'allabout'),
            'id' => 'sidebar-ir',  
            'deccription' => __('ir sidebar'),
            'class' => 'sidebar-ir',     
        );
        register_sidebar($sidebar4);

        /* Add Sidebar for guide */
        $sidebar5 = array(
            'name' => __('Guide Sidebar', 'allabout'),
            'id' => 'sidebar-guide',  
            'deccription' => __('guide sidebar'),
            'class' => 'sidebar-guide',     
        );
        register_sidebar($sidebar5);

        /* Add Sidebar for adinfo */
        $sidebar6 = array(
            'name' => __('Adinfo Sidebar', 'allabout'),
            'id' => 'sidebar-adinfo',  
            'deccription' => __('adinfo sidebar'),
            'class' => 'sidebar-adinfo',     
        );
        register_sidebar($sidebar6);

        /* Add Sidebar for corporate_press */
        $sidebar7 = array(
            'name' => __('Corporatepress Sidebar', 'allabout'),
            'id' => 'corporatepress-adinfo',  
            'deccription' => __('corporatepress sidebar'),
            'class' => 'corporatepress-adinfo',     
        );
        register_sidebar($sidebar7);

        /* Add Sidebar for inquiry */
        $sidebar8 = array(
            'name' => __('Inquiry Sidebar', 'allabout'),
            'id' => 'sidebar-inquiry',  
            'deccription' => __('inquiry sidebar'),
            'class' => 'sidebar-inquiry',     
        );
        register_sidebar($sidebar8);

    }
    add_action('init' , 'allabout_theme_setup');
}


//==================================================
//  ▼   Get Sidebar by ID
//==================================================
function is_tree( $pid ) {     
    global $post;              
    if ( is_page($pid) )
        return TRUE;           
    $anc = get_post_ancestors( $post->ID );
    foreach ( $anc as $ancestor ) {
        if( is_page() && $ancestor == $pid ) {
            return TRUE;
        }
    }
    return FALSE;  
}

//==================================================
//  ▼   Remove only the code which creates the css
//==================================================
add_filter( 'the_content', 'remove_br_gallery', 11, 2);
function remove_br_gallery($output) {
    return preg_replace('/<br style=(.*)>/mi','',$output);
}

//==================================================
//  ▼   Remove tag <br> in post
//==================================================
function better_wpautop($pee){
return wpautop($pee,false);
}

remove_filter( 'the_content', 'wpautop' );
add_filter( 'the_content', 'better_wpautop' , 99);
add_filter( 'the_content', 'shortcode_unautop',100 );



//==================================================
//  ▼  Setting navigation
//==================================================
if(!function_exists('allabout_menu')) {
    function allabout_menu($menu){
        $menu = array(
            'menu' => $menu,
            'theme_location' => $menu,
        );
        wp_nav_menu($menu);
    }
}


//=================================
//  ▼   GET MENU SIDEBAR
//=================================
function register_my_menu() {
    register_nav_menu('header-menu',__( 'Menu main' ));
}
add_action( 'init', 'register_my_menu' );


//=================================
//  ▼   CPT PICKUP
//=================================
function custom_post_type()
{
    $label = array(
        'name' => 'Pickup', 
    );
    $args = array(
        'labels' => $label, 
        'description' => 'Post type pickup',
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'author',
            'thumbnail',
            'comments',
            'trackbacks',
            'revisions',
            'custom-fields'
        ), 
        'taxonomies' => array( '', 'post_tag' ),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 6,
        'menu_icon' => 'dashicons-portfolio',
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post' 
    );
    register_post_type('pickup', $args); 
}
add_action('init', 'custom_post_type');



//=================================
//  ▼   CATEGORY PICKUP
//=================================
function create_pickup_taxonomies() {
    $labels = array(
        'name'              => _x( 'Chuyên mục', 'taxonomy general name' ),
        'singular_name'     => _x( 'Pickup Category', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Categories' ),
        'all_items'         => __( 'All Categories' ),
        'parent_item'       => __( 'Parent Category' ),
        'parent_item_colon' => __( 'Parent Category:' ),
        'edit_item'         => __( 'Edit Category' ),
        'update_item'       => __( 'Update Category' ),
        'add_new_item'      => __( 'Add New Category' ),
        'new_item_name'     => __( 'New Category Name' ),
        'menu_name'         => __( 'Categories' ),
    );

    $args = array(
        'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'pickup_category' ),
    );

    register_taxonomy( 'pickup_categories', array( 'pickup' ), $args );
}
add_action( 'init', 'create_pickup_taxonomies', 0 );


//=================================
//  ▼   CPT IR
//=================================
function custom_post_type2()
{
    $label = array(
        'name' => 'Ir', 
    );
    $args = array(
        'labels' => $label, 
        'description' => 'Post type ir',
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'author',
            'thumbnail',
            'comments',
            'trackbacks',
            'revisions',
            'custom-fields'
        ), 
        'taxonomies' => array( '', 'post_tag' ),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 6,
        'menu_icon' => 'dashicons-portfolio',
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post' 
    );
    register_post_type('ir', $args); 
}
add_action('init', 'custom_post_type2');


//=================================
//  ▼   CATEGORY PICKUP
//=================================
function create_IR_taxonomies() {
    $labels = array(
        'name'              => _x( 'Chuyên mục', 'taxonomy general name' ),
        'singular_name'     => _x( 'Ir Category', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Categories' ),
        'all_items'         => __( 'All Categories' ),
        'parent_item'       => __( 'Parent Category' ),
        'parent_item_colon' => __( 'Parent Category:' ),
        'edit_item'         => __( 'Edit Category' ),
        'update_item'       => __( 'Update Category' ),
        'add_new_item'      => __( 'Add New Category' ),
        'new_item_name'     => __( 'New Category Name' ),
        'menu_name'         => __( 'Categories' ),
    );

    $args = array(
        'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'ir_category' ),
    );

    register_taxonomy( 'ir_categories', array( 'ir' ), $args );
}
add_action( 'init', 'create_ir_taxonomies', 0 );


//=================================
//  ▼  BREADCRUMBS
//=================================
function custom_breadcrumbs() {
    $breadcrums_id      = 'bearcum';
    $breadcrums_class   = 'bearcum';
    $home_title         = 'Home';
      
    // Get taxonomy Holiday
    $custom_taxonomy    = 'holiday_categories';
       
    // Get the query & post information
    global $post,$wp_query;
       
    // Do not display on the homepage
    if ( !is_front_page() ) {
       
        // Build the breadcrums
        echo '<div class="' . $breadcrums_class . '">';
        echo '<ul>';
        
        // Home page
        echo '<li class="home"><a href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';

        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
              
            echo '<li>' . post_type_archive_title($prefix, false) . '</li>';
              
        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
              
            // Get post custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li><a href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
              
            }
              
            $custom_tax_name = get_queried_object()->name;
            echo '<li>' . $custom_tax_name . '</li>';
              
        } else if ( is_single() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li><a href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
              
            }
              
            // Get post category info
            $category = get_the_category();
             
            if(!empty($category)) {
              
                // Get last category post is in
                $last_category = end(array_values($category));
                  
                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);
                  
                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= '<li>'.$parents.'</li>';
                }
             
            }
              
            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                   
                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
               
            }
              
            // Check if the post is in a category
            if(!empty($last_category)) {
                echo $cat_display;
                echo '<li>' . get_the_title() . '</li>';
                  
            // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {
                echo '<li>' . get_the_title() . '</li>';
              
            }
              
        } else if ( is_category() ) {
               
            // Category page
            echo '<li >' . single_cat_title('', false) . '</li>';
               
        } else if ( is_page() ) {
               
            // Standard page
            if( $post->post_parent ){
                   
                // If child page, get parents 
                $anc = get_post_ancestors( $post->ID );
                   
                // Get parents in the right order
                $anc = array_reverse($anc);
                   
                // Parent page loop
                if ( !isset( $parents ) ) $parents = null;
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li><a href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                }
                   
                // Display parent pages
                echo $parents;
                   
                // Current page
                echo '<li> ' . get_the_title() . '</li>';
                   
            } else {
                   
                // Just display current page if not parents
                echo '<li> ' . get_the_title() . '</li>';
                   
            }
        } 
        echo '</ul>';
        echo '</div>';   
    }  
}

//=================================
//  ▼   Add Ajax
//=================================

add_action( 'wp_head', 'add_demo_ajaxurl', 1 );
function add_demo_ajaxurl() {
?>
    <script>
        var ajaxurl = '<?php echo admin_url( 'admin-ajax.php'); ?>';
    </script>
<?php
}

//ajaxの呼び出し先
function ajax_get_page() {

    $args = array( 
        'post_status' => 'publish',
    );
    $query = new WP_Query( $args );
    $posts = $query->get_posts();

    $output = array();
    foreach( $posts as $post ) {
        $theID = $post->ID;
        $theTitle = $post->post_title;
        $theDate = get_the_date( 'Y年m月d日', $post );
        $theYear = get_the_date( 'Y', $post );
        $postCat = get_the_category( $post->ID );
        $postCatName = $postCat[0]->name;
        $companyName = get_post_meta($post->ID, "company", true);
        $postLink = get_post_permalink($post->ID);

        $output[] = array(
            'id' => $theID,
            'title' =>  $theTitle,
            'time' => $theDate,
            'year' => $theYear,
            'category' =>  $postCatName,
            'postLink' => $postLink,
            'companyName' => $companyName
        );
    }
    echo json_encode($output, JSON_UNESCAPED_UNICODE);

    die();

}
add_action( 'wp_ajax_ajax_get_page', 'ajax_get_page' );
add_action( 'wp_ajax_nopriv_ajax_get_page', 'ajax_get_page' );



// register Yearly_Widget
add_action( 'widgets_init', function(){
    register_widget( 'yearly_widget' );
});

class yearly_widget extends WP_Widget {
    // class constructor
    public function __construct() {
        $widget_ops = array( 
            'classname' => 'yearly_widget',
            'description' => 'Show Archives By Year',
        );
        parent::__construct( 'yearly_widget', 'Yearly Widget', $widget_ops );
    }
    
    // output the option form field in admin Widgets screen
    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'Title', 'text_domain' );
        ?>
        <p>
        <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
        <?php esc_attr_e( 'Title:', 'text_domain' ); ?>
        </label> 
        
        <input 
            class="widefat" 
            id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" 
            name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" 
            type="text" 
            value="<?php echo esc_attr( $title ); ?>">
        </p>
        <?php
    }

    // save options
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

        return $instance;
    }

    // widget front end
    public function widget( $args, $instance ) {
        echo $args['before_widget'];
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
        }

         $args = array(
            'type'            => 'yearly',
            'limit'           => 99,
            'format'          => 'html', 
            'show_post_count' => false,
            'echo'            => 1,
            'order'           => 'DESC',
            'post_type'     => 'post'
        );

        echo '<ul class="listYear">'; 
        wp_get_archives( $args );
        echo '</ul>';

        echo $args['after_widget'];
    }
}



?>