<?php get_header(); ?>
	<!--▼ Main ▼-->
   <main class="main">
      <!-- Listservice -->
      <nav class="listservice subPage">
        <?php allabout_menu('menu_serviceList'); ?>
      </nav>
      <div class="content clearfix">
         <!-- Breacrum star -->
         <?php custom_breadcrumbs(); ?>

         <!-- Content -->
         <div class="mainContent">
            <div class="search-info">
               <?php 
                  $search_query = new WP_Query('s='.$s.'&showpost=-1');
                  $search_keyword = wp_specialchars($s,1);
                  $search_count = $search_query->post_count;

                  printf(__('We found %1$s articles for your search.','allbout'),$search_count);
               ?>
            </div>
            <?php if( have_posts()) : while (have_posts()) : the_post(); ?>
               <?php get_template_part('content', get_post_format()); ?>
            <?php endwhile ?>
            <?php else: ?>
               <?php get_template_part('content', 'none') ?>
            <?php endif; ?>
         </div>
      </div>
   </main>
   <!--▲ Main ▲-->
<?php get_footer(); ?>