<!DOCTYPE html>
<html <?php language_attributes(); ?> />

<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="canonical" href=/index.html />
  <link href="<?php echo THEME_URL ?>/assets/css/common.css" rel="stylesheet">
  <link href="<?php echo THEME_URL ?>/assets/css/top.css" rel="stylesheet">
  <link href="<?php echo THEME_URL ?>/assets/css/corporate.css" rel="stylesheet">
  <link href="<?php echo THEME_URL ?>/assets/css/corporate_press.css" rel="stylesheet">
  <link href="<?php echo THEME_URL ?>/assets/css/adinfo.css" rel="stylesheet">
  <link href="<?php echo THEME_URL ?>/assets/css/service.css" rel="stylesheet">
  <link href="<?php echo THEME_URL ?>/assets/css/recruit.css" rel="stylesheet">
  <link href="<?php echo THEME_URL ?>/assets/css/ir.css" rel="stylesheet">
  <link href="<?php echo THEME_URL ?>/assets/css/guide.css" rel="stylesheet">
  <link href="<?php echo THEME_URL ?>/assets/css/social.css" rel="stylesheet">
  <link href="<?php echo THEME_URL ?>/assets/css/slick.css" rel="stylesheet">
  <?php wp_head(); ?>
</head>

<body class="wrapper">
   <!--▼ Header ▼-->
   <header class="header">
   <div class="header__inner clearfix">
      <h1 class="header__logo"><a href="<?php echo esc_url( home_url( '/home' ) ); ?>">All About</a></h1>
      <nav>
         <?php allabout_menu('menu_top');?>
      </nav>
   </div>
   </header>
   <!--▲ Header ▲-->