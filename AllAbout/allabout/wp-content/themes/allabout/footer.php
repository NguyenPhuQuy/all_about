   <!--▼ Footer ▼-->
   <div class="footer">
      <div class="content">
         <!-- Nav footer left -->
         <?php allabout_menu('menu-footer-left');?>
         <!-- Nav footer center left -->
         <?php allabout_menu('menu-footer-center-left'); ?>
         <!-- Nav footer center right -->
         <?php allabout_menu('menu-footer-center-right'); ?>
         <!-- Nav footer right -->
         <?php allabout_menu('menu-footer-right'); ?>
         <!-- Footer coppyright -->
         <div class="footer__coppyright">
            <small>Copyright &copy; <?php bloginfo('sitename'); ?>, Inc. All rights reserved.</small>
         </div>
         <!-- Go Top -->
         <div class="pageTop">
            <span>トップへ戻る</span>
         </div>   
      </div>
   </div>
   <!--▲ Footer ▲-->
   <?php wp_footer(); ?>
   <script src="<?php echo THEME_URL ?>/assets/js/jquery-3.3.1.js"></script>
   <script src="<?php echo THEME_URL ?>/assets/js/slick.js"></script>
   <script src="<?php echo THEME_URL ?>/assets/js/common.js"></script>
   <script src="<?php echo THEME_URL ?>/assets/js/modalBox.js"></script>

   <!-- AJAX Code -->
   <script>

    function createHtmlContent(arr) {
        $('#ajaxRoot').html('');

        var list = $('<ul class="news__lists"></ul>');

        if (arr[0]) {
            $.each(arr, function(){
                var listItem = $('<li></li>');
                var postLink = $('<a href="' + this.postLink + '" class="title"></a>');
                var postCat = $('<span class="news__cat"></span>');
                var postTime = $('<span class="news__date"></span>');
                var postCompany = $('<span class="news__company"></span>');
                var newsBox = $('<div class="news__box"></div>');
                var postLabel = $('<span class="news__label">New</span>');

                postTime.html(this.time).appendTo(listItem);
                postCat.html(this.category).appendTo(listItem);
                postCompany.html(this.companyName).appendTo(listItem);

                postLink.html(this.title).appendTo(newsBox);
                postLabel.appendTo(newsBox);

                newsBox.appendTo(listItem);

                listItem.appendTo(list);
            })
            list.appendTo($('#ajaxRoot'));
        } else {
            $('#ajaxRoot').html('<div class="failure">Fail To Get Post, No Post Exist</div>');
        }

    }

    var resData;

    function ajaxPost() {
        var ajaxurl = '<?php echo admin_url( 'admin-ajax.php'); ?>';

        return $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                'action' : 'ajax_get_page'
            },
            success: function( response ){
                resData = JSON.parse(response);
                return resData;
            }
        });

    }

    $(function(){

        $.when(ajaxPost()).done(function(){
            $('#searchSubmit').click(function(e){
                e.preventDefault();
                var year = $('#postYear').val();
                var catName = $('#postCat').val();
                var companyName = $('#companyName').val();
                
                var result = [];

                $.each(resData, function(){
                    if (
                        this.category == catName
                        ||
                        this.companyName == companyName
                        ||
                        this.year == year
                    ) {
                        result.push(this);
                    }
                })

                console.log(result);

                createHtmlContent(result);
            })

            // Filter By Sidebar Link
            $('.sidebar .yearly_widget a').each(function(){
                $(this).text( $(this).text() + '年度' );
                $(this).click(function(e){
                    e.preventDefault();
                    var year = $(this).text().substr(0, 4);

                    var result = [];
                    $.each(resData, function(){
                        if ( this.year == year ) {
                            result.push(this);
                        }
                    })
                    console.log(result);

                    createHtmlContent(result);
                })
            })

        }) 

    });
   </script>
   
   <!-- Ajax Code -->
</body>
</html>