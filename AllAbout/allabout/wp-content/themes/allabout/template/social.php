<?php /* Template Name: Social */ ?>
<?php get_header(); ?>
   <!--▼ Main ▼-->
   <main class="main" id="corporate">
      <!-- Listservice -->
      <nav class="listservice subPage">
        <?php allabout_menu('menu_serviceList'); ?>
      </nav>
      <div class="content clearfix">
         <!-- Breacrum star -->
         <?php custom_breadcrumbs(); ?>
         <!-- Content -->
         <div class="socialmedia">
            <!-- Title -->
            <div class="subTitle">
               <h2><?php the_title(); ?></h2>
            </div>
            <div class="socialmedia__content clearfix">
               <div class="socialmedia__item">
                  <figure>
                     <img src="<?php echo THEME_URL ?>/assets/images/socialmedia/socialMediaLogo_pr.png" alt="" />
                     <figcaption>
                        <h4>All About広報</h4>
                        <p>オールアバウト広報の公式アカウントです。プレスリリースや約900名のガイド（＝専門家）のオススメ記事を配信中。</p>
                        <ul class="socialmedia__btnList">
                           <li><a class="twitter" href="https://twitter.com/allabout_pr" target="_blank"></a></li>
                        </ul>
                     </figcaption>
                  </figure>
               </div>

               <div class="socialmedia__item">
                  <figure>
                     <img src="<?php echo THEME_URL ?>/assets/images/socialmedia/socialMediaLogo_pr.png" alt="" />
                     <figcaption>
                        <h4>オールアバウト採用担当</h4>
                        <p>オールアバウトの2016年度新卒者向け公式アカウントです。学生の皆様に役立つ記事や募集状況などについてお知らせします。</p>
                        <ul class="socialmedia__btnList">
                           <li><a class="facebook" href="https://www.facebook.com/allabout.recruit" target="_blank"></a></li>
                           <li><a class="twitter" href="https://twitter.com/allabout_job" target="_blank"></a></li>
                        </ul>
                     </figcaption>
                  </figure>
               </div>

               <div class="socialmedia__item">
                  <figure>
                     <img src="<?php echo THEME_URL ?>/assets/images/socialmedia/socialMediaLogo01.png" alt="" />
                  </figure>
                  <figcaption>
                     <h4>All About（オールアバウト）</h4>
                     <p>その道のプロがあなたをガイドする総合情報サイト「All About」の公式アカウントです。</p>
                     <ul class="socialmedia__btnList">
                        <li><a class="facebook" href="https://www.facebook.com/allabout.co.jp" target="_blank"></a></li>
                        <li><a class="twitter" href="https://twitter.com/allabout_news" target="_blank"></a></li>
                        <li><a class="google" href="https://plus.google.com/117439710380669558122" target="_blank"></a></li>
                     </ul>
                  </figcaption>
               </div>

               <div class="socialmedia__item">
                  <figure>
                     <img src="<?php echo THEME_URL ?>/assets/images/socialmedia/socialMediaLogo04.png" alt="" />
                  </figure>
                  <figcaption>
                     <h4>All About For M</h4>
                     <p>熱い男たちに必要なファッション、ビューティ、グルメなどをデイリーでお届けするFor Mの公式アカウント。</p>
                     <ul class="socialmedia__btnList">
                        <li><a class="facebook" href="https://www.facebook.com/AllAbout.ForM" target="_blank"></a></li>
                        <li><a class="twitter" href="https://twitter.com/allabout_men" target="_blank"></a></li>
                     </ul>
                  </figcaption>
               </div>

               <div class="socialmedia__item">
                  <figure>
                     <img src="<?php echo THEME_URL ?>/assets/images/socialmedia/nTuDiCyJ_400x400.jpg" alt="" />
                  </figure>
                  <figcaption>
                     <h4>All About News</h4>
                     <p>専門家しか知らないトレンドやニュース、専門家ならではの時事解説など、ここでしか読めないニュースをお届けします。</p>
                     <ul class="socialmedia__btnList">
                        <li><a class="twitter" href="https://twitter.com/All_About__NEWS" target="_blank"></a></li>
                     </ul>
                  </figcaption>
               </div>

               <div class="socialmedia__item">
                  <figure>
                     <img src="<?php echo THEME_URL ?>/assets/images/socialmedia/socialMediaLogo12.png" alt="" />
                  </figure>
                  <figcaption>
                     <h4>All About まとめコンテンツ</h4>
                     <p>総合情報サイトAll Aboutが作る、まとめコンテンツの公式Twitterです。</p>
                     <ul class="socialmedia__btnList">
                        <li><a class="twitter" href="https://twitter.com/allabout_matome" target="_blank"></a></li>
                     </ul>
                  </figcaption>
               </div>

               <div class="socialmedia__item">
                  <figure>
                     <img src="<?php echo THEME_URL ?>/assets/images/socialmedia/socialMediaLogo99.png" alt="" />
                  </figure>
                  <figcaption>
                     <h4>All About 新着記事</h4>
                     <p>様々なジャンルの専門家が執筆した最新記事をリアルタイムでお届けします。</p>
                     <ul class="socialmedia__btnList">
                        <li><a class="twitter" href="https://twitter.com/allabout_list" target="_blank"></a></li>
                     </ul>
                  </figcaption>
               </div>

               <div class="socialmedia__item">
                  <figure>
                     <img src="<?php echo THEME_URL ?>/assets/images/socialmedia/socialMediaLogo_money.png" alt="" />
                  </figure>
                  <figcaption>
                     <h4>All About マネー</h4>
                     <p>「All About マネー」の公式Twitterです。初心者向けに、お金の貯め方・増やし方など、人生に役立つマネー情報をお届けします。</p>
                     <ul class="socialmedia__btnList">
                        <li><a class="twitter" href="https://twitter.com/aa_finance" target="_blank"></a></li>
                     </ul>
                  </figcaption>
               </div>

               <div class="socialmedia__item">
                  <figure>
                     <img src="<?php echo THEME_URL ?>/assets/images/socialmedia/socialMediaLogo_kurashi.png" alt="" />
                  </figure>
                  <figcaption>
                     <h4>All About 暮らし</h4>
                     <p>「All About 暮らし」の公式アカウントです。レシピや家事テクニックなど毎日の生活に役立つ情報をお届けします。</p>
                     <ul class="socialmedia__btnList">
                        <li><a class="twitter" href="https://twitter.com/allabout_recipe" target="_blank"></a></li>
                     </ul>
                  </figcaption>
               </div>

               <div class="socialmedia__item">
                  <figure>
                     <img src="<?php echo THEME_URL ?>/assets/images/socialmedia/socialMediaLogo_kosodate.png" alt="" />
                  </figure>
                  <figcaption>
                     <h4>All About 子育て</h4>
                     <p>「All About 子育て」の公式Twitterです。子育てのヒントや便利なグッズに関するお役立ち情報をお届けします。</p>
                     <ul class="socialmedia__btnList">
                        <li><a class="twitter" href="https://twitter.com/allabout_kids" target="_blank"></a></li>
                     </ul>
                  </figcaption>
               </div>

               <div class="socialmedia__item">
                  <figure>
                     <img src="<?php echo THEME_URL ?>/assets/images/socialmedia/twitter_icon_beauty_socialM.png" alt="" />
                  </figure>
                  <figcaption>
                     <h4>All About ビューティ</h4>
                     <p>「All About ビューティ」の公式アカウントです。本当に知りたかった美容テクをお届けします。</p>
                     <ul class="socialmedia__btnList">
                        <li><a class="facebook" href="https://www.facebook.com/allaboutmico" target="_blank"></a></li>
                     </ul>
                  </figcaption>
               </div>

               <div class="socialmedia__item">
                  <figure>
                     <img src="<?php echo THEME_URL ?>/assets/images/socialmedia/socialMediaLogo_diet.png" alt="" />
                  </figure>
                  <figcaption>
                     <h4>All About ダイエット</h4>
                     <p>「All About ダイエット」の情報を、ダイエットなうな担当者が日々お知らせ♪</p>
                     <ul class="socialmedia__btnList">
                        <li><a class="twitter" href="https://twitter.com/allabout_diet" target="_blank"></a></li>
                     </ul>
                  </figcaption>
               </div>

               <div class="socialmedia__item">
                  <figure>
                     <img src="<?php echo THEME_URL ?>/assets/images/socialmedia/twitter_icon_healthcare.png" alt="" />
                  </figure>
                  <figcaption>
                     <h4>All About 健康・医療</h4>
                     <p>「All About 健康・医療」の公式アカウントです。あなたの健康的な毎日をサポートする情報を配信します。</p>
                     <ul class="socialmedia__btnList">
                        <li><a class="twitter" href="https://twitter.com/AA_healthcare" target="_blank"></a></li>
                     </ul>
                  </figcaption>
               </div>

               <div class="socialmedia__item">
                  <figure>
                     <img src="<?php echo THEME_URL ?>/assets/images/socialmedia/socialMediaLogo_love.png" alt="" />
                  </figure>
                  <figcaption>
                     <h4>All About 恋愛</h4>
                     <p>「All About恋愛」の最新コラムを紹介。脱・不毛な恋愛、モテナイ日々。恋愛情報届けます。</p>
                     <ul class="socialmedia__btnList">
                        <li><a class="twitter" href="https://twitter.com/allabout_love" target="_blank"></a></li>
                     </ul>
                  </figcaption>
               </div>

               <div class="socialmedia__item">
                  <figure>
                     <img src="<?php echo THEME_URL ?>/assets/images/socialmedia/socialMediaLogo_gourmet.png" alt="" />
                  </figure>
                  <figcaption>
                     <h4>All About グルメ</h4>
                     <p>「All About グルメ」の公式Twitterです。フレンチから日本酒まで、食べ歩きの達人が選りすぐりのグルメ情報を紹介します。</p>
                     <ul class="socialmedia__btnList">
                        <li><a class="twitter" href="https://twitter.com/allaboutgourmet" target="_blank"></a></li>
                     </ul>
                  </figcaption>
               </div>
            </div>
         </div>
      </div>
   </main>
   <!--▲ Main ▲-->
<?php get_footer(); ?>