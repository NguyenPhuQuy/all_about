<?php /* Template Name: Corporate_press */ ?>
<?php get_header(); ?>
   <!--▼ Main ▼-->
   <main class="main" id="corporate">
      <!-- Listservice -->
      <nav class="listservice subPage">
        <?php allabout_menu('menu_serviceList'); ?>
      </nav>
      <div class="content clearfix">
         <!-- Breacrum star -->
         <?php custom_breadcrumbs(); ?>
         <!-- Sidebar -->
         <?php get_sidebar(); ?>
         <!-- Content -->
         <div class="mainContent corporate">
            <section class="mainContent__inner">
               <!-- Title -->
               <div class="subTitle">
                  <h2><?php the_title(); ?></h2>
               </div>
               <?php get_template_part('searchform' ); ?> 
               <div class="news" id="ajaxRoot">
                    <ul class="news__lists">
               <?php
                  $args_new = array(
                     'posts_per_page' => 9999,
                     'post_status' => 'publish',
                  );
                  $the_query_new = new WP_Query( $args_new );

                  if ( $the_query_new->have_posts() ) :

                  while ( $the_query_new->have_posts() ) : $the_query_new->the_post();

                  ?>
                     <li>
                        <span class="news__date"><?php echo get_the_date('Y年m月d日'); ?></span>
                        <?php
                           $categories_new = get_the_category();
                           if ( ! empty( $categories_new ) ) {
                               echo '<span class="news__cat">' . esc_html( $categories_new[0]->name ) . '</span>';
                           }
                        ?>
                        <span class="news__company"><?php the_field('company'); ?></span> 
                        <div class="news__box">
                           <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                           <span class="news__label"><?php the_field('label'); ?></span>
                        </div>
                     </li>
                  <?php

                  endwhile;

                  endif;

                  wp_reset_postdata();

                  ?>
               </li>
            </ul>
               </div>
            </section>
         </div>
      </div>
   </main>
   <!--▲ Main ▲-->
<?php get_footer(); ?>