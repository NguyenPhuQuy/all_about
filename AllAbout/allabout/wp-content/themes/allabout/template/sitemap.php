<?php /* Template Name: Sitemap */ ?>
<?php get_header(); ?>
   <main class="main">
      <!-- Listservice -->
       <nav class="listservice subPage">
           <?php allabout_menu('menu_serviceList'); ?>
       </nav>
      <div class="content clearfix">
         <!-- Breacrum star -->
            <?php custom_breadcrumbs(); ?>
            <!-- Title -->
           <div class="subTitle">
              <h2><?php the_title(); ?></h2>
           </div>
         <div class="sitemap">
            <?php if( have_posts()) : while (have_posts()) : the_post(); ?>
               <?php the_content(); ?>
            <?php endwhile ?>
            <?php endif; ?>
         </div>
      </div>
   </main>

<?php get_footer(); ?>

