<?php /* Template Name: Ir */ ?>
<?php get_header(); ?>
   <!--▼ Main ▼-->
   <main class="main">
      <!-- Listservice -->
      <nav class="listservice subPage">
        <?php allabout_menu('menu_serviceList'); ?>
      </nav>
      <div class="content clearfix">
         <!-- Breacrum star -->
         <?php custom_breadcrumbs(); ?>
         <!-- Sidebar -->
         <?php get_sidebar(); ?>
         <!-- Content -->
         <div class="mainContent ir">
            <section class="mainContent__inner">
               <!-- Title -->
               <div class="subTitle">
                  <h2><?php the_title(); ?></h2>
               </div>
               <div class="ir__content">
                  <div class="ir__linlListBox">
                     <h3 class="ttl"><a href="http://localhost/allabout/ir%ef%bd%a5%e6%8a%95%e8%b3%87%e5%ae%b6%e6%83%85%e5%a0%b1/ir%e3%83%8b%e3%83%a5%e3%83%bc%e3%82%b9/">IRニュース</a></h3>
                     <ul class="newList">
                        <?php
                           $args_new = array(
                              'posts_per_page' => 6,
                              'post_status' => 'publish',
                              'post_type' => 'ir', 
                           );
                           $the_query_new = new WP_Query( $args_new );

                           if ( $the_query_new->have_posts() ) :

                           while ( $the_query_new->have_posts() ) : $the_query_new->the_post();

                           ?>
                              <li>
                                 <span class="time"><?php echo get_the_date('Y年m月d日'); ?></span>
                                 <div class="newsContent">
                                    <a class="pdf" href="<?php the_field('link_ir'); ?>" target="_blank"><?php echo the_title(); ?></a>
                                 </div>
                              </li>
                           <?php

                           endwhile;

                           endif;

                           wp_reset_postdata();

                           ?>
                     </ul>
                  </div>
                  <?php the_content(); ?>
               </div>
            </section>
         </div>
      </div>
   </main>
   <!--▲ Main ▲-->
<?php get_footer(); ?>