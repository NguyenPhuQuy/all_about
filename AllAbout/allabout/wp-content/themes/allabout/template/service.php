<?php /* Template Name: Service */ ?>
<?php get_header(); ?>
   <!--▼ Main ▼-->
   <main class="main" id="corporate">
      <!-- Listservice -->
      <nav class="listservice subPage">
        <?php allabout_menu('menu_serviceList'); ?>
      </nav>
      <div class="content clearfix">
         <!-- Breacrum star -->
         <?php custom_breadcrumbs(); ?>
         <!-- Sidebar -->
         <?php get_sidebar(); ?>
         <!-- Content -->
         <div class="mainContent service">
            <section class="mainContent__inner">
               <!-- Title -->
               <div class="subTitle">
                  <h2><?php the_title(); ?></h2>
               </div>
               
               <div class="service__content">
                  <p class="service__text">
                     オールアバウトグループは専門家ネットワークを基盤として、コミュニケーションを軸に、既存の『情報流・商流・製造流』にイノベーションを図るプラットフォームを提供しています。総合情報サイト「All About」による「メディア事業」をはじめ、現在、動画・電子書籍を扱う「コンテンツ事業」、消費者と企業を繋ぐ「マーケティング支援事業」、「教育・学習事業」、「グローバル事業」そして「専門家ビジネス」といった6つの事業に紐づく様々なサービスをWEB&リアルで展開しています。
                  </p>
                  <ul class="service__listPage">
                     <?php the_content(); ?>
                  </ul>

                  <figure class="service__imgTop">
                     <img src="<?php echo THEME_URL ?>/assets/images/service/serviceImg_01.png" alt="" />
                  </figure>

                  <div class="service__imgLogo">
                     <ul class="service__listLogo">
                        <li class="img">
                           <img src="<?php echo THEME_URL ?>/assets/images/service/logo_allabout_single.png" alt="" />
                        </li>
                        <li class="img">
                           <img src="<?php echo THEME_URL ?>/assets/images/service/allabout_news.jpg" alt="" />
                        </li>
                        <li class="img">
                           <img src="<?php echo THEME_URL ?>/assets/images/service/allabout_mico_logo.jpg" alt="" />
                        </li>
                        <li class="img">
                           <img src="<?php echo THEME_URL ?>/assets/images/service/arujan.jpg" alt="" />
                        </li>
                        <li class="img">
                           <img src="<?php echo THEME_URL ?>/assets/images/service/dl_market.jpg" alt="" />
                        </li>
                        <li class="img">
                           <img src="<?php echo THEME_URL ?>/assets/images/service/form_posi_color.jpg" alt="" />
                        </li>
                        <li class="img">
                           <img src="<?php echo THEME_URL ?>/assets/images/service/logo-1024x820.png" alt="" />
                        </li>
                        <li class="img">
                           <img src="<?php echo THEME_URL ?>/assets/images/service/citrus.jpg" alt="" />
                        </li>
                        <li class="img">
                           <img src="<?php echo THEME_URL ?>/assets/images/service/sample_hyakkaten.png" alt="" />
                        </li><li class="img">
                           <img src="<?php echo THEME_URL ?>/assets/images/service/facebook_navi.png" alt="" />
                        </li>
                        </li><li class="img">
                           <img src="<?php echo THEME_URL ?>/assets/images/service/gakusyu.jpg" alt="" />
                        </li>
                        </li><li class="img">
                           <img src="<?php echo THEME_URL ?>/assets/images/service/picup_logo.png" alt="" />
                        </li>
                     </ul>
                  </div>

                  <figure class="service__imgBottom">
                     <img src="<?php echo THEME_URL ?>/assets/images/service/serviceImg_02.jpg" alt="" />
                  </figure>
               </div>
            </section>
         </div>
      </div>
   </main>
   <!--▲ Main ▲-->
<?php get_footer(); ?>