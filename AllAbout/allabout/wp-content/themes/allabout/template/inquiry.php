<?php /* Template Name: inquiry */ ?>
<?php get_header(); ?>
   <!--▼ Main ▼-->
   <main class="main">
      <!-- Listservice -->
      <nav class="listservice subPage">
        <?php allabout_menu('menu_serviceList'); ?>
      </nav>
      <div class="content clearfix">
         <!-- Breacrum star -->
         <?php custom_breadcrumbs(); ?>
         <!-- Sidebar -->
         <?php get_sidebar(); ?>
         <!-- Content -->
         <div class="mainContent service">
            <section class="mainContent__inner">
               <!-- Title -->
               <div class="subTitle">
                  <h2><?php the_title(); ?></h2>
               </div>
               <div class="service__content">
               <ul class="service__listPage">
                  <li>
                     <a href="http://localhost/allabout/%e3%81%8a%e5%95%8f%e5%90%88%e3%81%9b/%e7%b7%8f%e5%90%88%e3%81%ae%e3%81%8a%e5%95%8f%e5%90%88%e3%81%9b/" style="font-size:19px">総合のお問合せ</a>
                  </li>
                  <li>
                     <a href="http://localhost/allabout/%e3%81%8a%e5%95%8f%e5%90%88%e3%81%9b/%e5%8f%96%e6%9d%90%e3%83%bb%e5%ba%83%e5%a0%b1%e3%81%ab%e9%96%a2%e3%81%99%e3%82%8b%e3%81%8a%e5%95%8f%e5%90%88%e3%81%9b/" style="font-size:19px">取材・広報に関するお問合せ</a>
                  </li>
                  <li>
                     <a href="http://localhost/allabout/%E3%81%8A%E5%95%8F%E5%90%88%E3%81%9B/" style="font-size:19px">広告掲載・ビジネスに関するお問合せ</a>
                  </li>
                  <li>
                     <a href="https://sec.allabout.co.jp/guideapply/" target="_blank" style="font-size:19px">ガイド応募に関するお問合せ</a>
                  </li>
                  <li>
                     <a href="http://localhost/allabout/%e3%81%8a%e5%95%8f%e5%90%88%e3%81%9b/ir%e3%81%ab%e9%96%a2%e3%81%99%e3%82%8b%e3%81%8a%e5%95%8f%e5%90%88%e3%81%9b/" style="font-size:19px">IRに関するお問合せ</a>
                  </li>
               </ul>
            </div>
            </section>
         </div>
      </div>
   </main>
   <!--▲ Main ▲-->
<?php get_footer(); ?>