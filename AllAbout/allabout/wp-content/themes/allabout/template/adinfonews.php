<?php /* Template Name: adinfonews */ ?>
<?php get_header(); ?>
   <!--▼ Main ▼-->
   <main class="main" id="adinfo">
      <!-- Listservice -->
      <nav class="listservice subPage">
        <?php allabout_menu('menu_serviceList'); ?>
      </nav>
      <div class="content clearfix">
         <!-- Breacrum star -->
         <?php custom_breadcrumbs(); ?>
         <!-- Sidebar -->
         <?php get_sidebar(); ?>
         <!-- Content -->
         <div class="mainContent adinfo">
            <div class="mainContent__inner">
               <!-- Title -->
               <div class="subTitle">
                  <h2><?php the_title(); ?></h2>
               </div>
               <div class="adinfo__intro">
                  <h3>最新のお知らせや、広告関連のリリース情報です。</h3>
               </div>

               <section class="bdnocolor">
                  <h3>2019年度</h3>
                  <ul class="news__lists">
                     <?php
                        $args_new = array(
                           'post_status' => 'publish',
                           'cat' => 24,
                           'year' => 2019
                        );
                        $the_query_new = new WP_Query( $args_new );

                        if ( $the_query_new->have_posts() ) :

                        while ( $the_query_new->have_posts() ) : $the_query_new->the_post();

                        ?>
                           <li>
                              <span class="news__date"><?php echo get_the_date('Y年m月d日'); ?></span>
                              <div class="news__box">
                                 <a href="<?php the_permalink(); ?>" class="newsContent"><?php the_title(); ?></a>
                              </div>
                           </li>
                        <?php

                        endwhile;

                        endif;

                        wp_reset_postdata();

                        ?>
                     </li>
                  </ul>
               </section>

               <section class="bdnocolor">
                  <h3>2018年度</h3>
                  <ul class="news__lists">
                     <?php
                        $args_new = array(
                           'post_status' => 'publish',
                           'cat' => 24,
                           'year' => 2018
                        );
                        $the_query_new = new WP_Query( $args_new );

                        if ( $the_query_new->have_posts() ) :

                        while ( $the_query_new->have_posts() ) : $the_query_new->the_post();

                        ?>
                           <li>
                              <span class="news__date"><?php echo get_the_date('Y年m月d日'); ?></span>
                              <div class="news__box">
                                 <a href="<?php the_permalink(); ?>" class="newsContent"><?php the_title(); ?></a>
                              </div>
                           </li>
                        <?php

                        endwhile;

                        endif;

                        wp_reset_postdata();

                        ?>
                     </li>
                  </ul>
               </section>

               <section class="bdnocolor">
                  <h3>2017年度</h3>
                  <ul class="news__lists">
                     <?php
                        $args_new = array(
                           'post_status' => 'publish',
                           'cat' => 24,
                           'year' => 2017
                        );
                        $the_query_new = new WP_Query( $args_new );

                        if ( $the_query_new->have_posts() ) :

                        while ( $the_query_new->have_posts() ) : $the_query_new->the_post();

                        ?>
                           <li>
                              <span class="news__date"><?php echo get_the_date('Y年m月d日'); ?></span>
                              <div class="news__box">
                                 <a href="<?php the_permalink(); ?>" class="newsContent"><?php the_title(); ?></a>
                              </div>
                           </li>
                        <?php

                        endwhile;

                        endif;

                        wp_reset_postdata();

                        ?>
                     </li>
                  </ul>
               </section>
            </div>
         </div>
      </div>
   </main>
   <!--▲ Main ▲-->
<?php get_footer(); ?>