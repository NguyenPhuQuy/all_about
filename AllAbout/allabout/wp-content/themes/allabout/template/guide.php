<?php /* Template Name: Guide */ ?>
<?php get_header(); ?>
   <!--▼ Main ▼-->
   <main class="main" id="guide">
      <!-- Listservice -->
      <nav class="listservice subPage">
        <?php allabout_menu('menu_serviceList'); ?>
      </nav>
      <div class="content clearfix">
         <!-- Breacrum star -->
         <?php custom_breadcrumbs(); ?>
         <!-- Sidebar -->
         <?php get_sidebar(); ?>
         <!-- Content -->
         <div class="mainContent guide">
            <section class="mainContent__inner">
               <!-- Title -->
               <div class="subTitle">
                  <h2><?php the_title(); ?></h2>
               </div>
               <div class=" guide__content">
                  <?php the_content(); ?>
               </div>
            </section>
         </div>
      </div>
   </main>
   <!--▲ Main ▲-->
<?php get_footer(); ?>