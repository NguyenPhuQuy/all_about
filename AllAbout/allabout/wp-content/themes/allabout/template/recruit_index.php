<?php /* Template Name: Recruit_index */ ?>
<?php get_header(); ?>
   <!--▼ Main ▼-->
   <main class="main" id="recruit">
      <!-- Listservice -->
      <nav class="listservice subPage">
        <?php allabout_menu('menu_serviceList'); ?>
      </nav>
      <div class="recruit__banner">
         <div class="recruit__wrapper">
            <div class="item">
               <img src="<?php echo THEME_URL ?>/assets/images/recruit/slide_06.jpg" alt="" />
            </div>
            <div class="item">
               <img src="<?php echo THEME_URL ?>/assets/images/recruit/slide_01.jpg" alt="" />
            </div>
            <div class="item">
               <img src="<?php echo THEME_URL ?>/assets/images/recruit/slide_05.jpg" alt="" />
            </div>
            <div class="item">
               <img src="<?php echo THEME_URL ?>/assets/images/recruit/slide_03.jpg" alt="" />
            </div>
         </div>

         <div class="recruit__btnList">
            <a href="https://allabout.snar.jp/index.aspx">マイページ 新規登録</a>
            <a href="https://allabout.snar.jp/login.aspx">マイページ ログイン</a>
         </div>
      </div>

      <div class="recruit__news">
         <div class="content">
            <span class="label">News</span>
            <ul>
               <li>
                  <span class="date">2018年7月10日</span>
                  <span class="title">2020新卒採用スタートしました。</span>
               </li>
               <li>
                  <span class="date">2017年7月31日</span>
                  <span class="title"><a href="https://about.allabout.co.jp/" target="_blank">All Aboutのオウンドメディア、オープンしました。</a></span>
               </li>
            </ul>
         </div>
      </div>

      <div class="recruit__fresh">
         <div class="content">
            <div class="recruit__social">
               <ul>
                  <li><a class="btn twiter" href="https://twitter.com/intent/tweet?original_referer=https%3A%2F%2Fcorp.allabout.co.jp%2Frecruit%2Ffresh%2Findex%2F&ref_src=twsrc%5Etfw&text=%E6%A0%AA%E5%BC%8F%E4%BC%9A%E7%A4%BE%E3%82%AA%E3%83%BC%E3%83%AB%E3%82%A2%E3%83%90%E3%82%A6%E3%83%88%20%E6%96%B0%E5%8D%92%E6%8E%A1%E7%94%A8&tw_p=tweetbutton&url=https%3A%2F%2Fcorp.allabout.co.jp%2Frecruit%2Ffresh%2Findex%2F" target="_blank">ツイート</a></li>
               </ul>
            </div>

            <div class="recruit__ownedmedia">
               <a href="https://about.allabout.co.jp/" target="_blank">
                  <img src="<?php echo THEME_URL ?>/assets/images/recruit/ownedmedia_banner.png" alt="" />
               </a>
            </div>

            <div class="recruit__cntList clearfix">
               <div class="cntSmall">
                  <a href="https://corp.allabout.co.jp/recruit/fresh/index/philosophy.html">
                     <figure>
                        <img src="<?php echo THEME_URL ?>/assets/images/recruit/fresh_1.jpg" alt="" />
                        <div class="text">
                           <div class="figcation">社長メッセージ</div>
                           <p class="arw"><img src="<?php echo THEME_URL ?>/assets/images/recruit/arw_r_01.png" alt="" /></p>
                        </div>
                     </figure>
                  </a>
               </div>

               <div class="cntLarge fl_r mr0">
                  <a href="https://corp.allabout.co.jp/recruit/fresh/index/member/">
                     <figure>
                        <img src="<?php echo THEME_URL ?>/assets/images/recruit/fresh_2.jpg" alt="" />
                        <div class="text">
                           <div class="figcation">社員インタビュー<br><small class="small">- 僕らが語るオールアバウトの魅力 -</small></div>
                           <p class="arw"><img src="<?php echo THEME_URL ?>/assets/images/recruit/arw_r_01.png" alt="" /></p>
                        </div>
                     </figure>
                  </a>
               </div>

               <div class="cntSmall">
                  <a href="https://corp.allabout.co.jp/recruit/fresh/index/recruit_philosophy.html">
                     <figure>
                        <img src="<?php echo THEME_URL ?>/assets/images/recruit/fresh_3.jpg" alt="" />
                        <div class="text">
                           <div class="figcation">採用理念</div>
                           <p class="arw"><img src="<?php echo THEME_URL ?>/assets/images/recruit/arw_r_01.png" alt="" /></p>
                        </div>
                     </figure>
                  </a>
               </div>

               <div class="cntTall">
                  <a href="https://corp.allabout.co.jp/recruit/fresh/index/information.html">
                     <figure>
                        <img src="<?php echo THEME_URL ?>/assets/images/recruit/fresh_4.jpg" alt="" />
                        <div class="text">
                           <div class="figcation">採用情報</div>
                           <p class="arw"><img src="<?php echo THEME_URL ?>/assets/images/recruit/arw_r_01.png" alt="" /></p>
                        </div>
                     </figure>
                  </a>
               </div>

               <div class="cntTall">
                  <a href="https://corp.allabout.co.jp/recruit/fresh/index/1week_00.html">
                     <figure>
                        <img src="<?php echo THEME_URL ?>/assets/images/recruit/fresh_5.jpg" alt="" />
                        <div class="text">
                           <div class="figcation">社員の1週間</div>
                           <p class="arw"><img src="<?php echo THEME_URL ?>/assets/images/recruit/arw_r_01.png" alt="" /></p>
                        </div>
                     </figure>
                  </a>
               </div>

               <div class="cntSmall mr0">
                  <a href="https://corp.allabout.co.jp/recruit/fresh/index/movie.html">
                     <figure>
                        <img src="<?php echo THEME_URL ?>/assets/images/recruit/fresh_6.jpg" alt="" />
                        <div class="text">
                           <div class="figcation">会社説明会動画</div>
                           <p class="arw"><img src="<?php echo THEME_URL ?>/assets/images/recruit/arw_r_01.png" alt="" /></p>
                        </div>
                     </figure>
                  </a>
               </div>

               <div class="cntSmall mr0">
                  <a href="https://corp.allabout.co.jp/recruit/fresh/index/engineer.html">
                     <figure>
                        <img src="<?php echo THEME_URL ?>/assets/images/recruit/fresh_7.jpg" alt="" />
                        <div class="text">
                           <div class="figcation">エンジニア環境</div>
                           <p class="arw"><img src="<?php echo THEME_URL ?>/assets/images/recruit/arw_r_01.png" alt="" /></p>
                        </div>
                     </figure>
                  </a>
               </div>

               <div class="cntLarge fl_l">
                  <a href="https://corp.allabout.co.jp/recruit/fresh/index/office.html">
                     <figure>
                        <img src="<?php echo THEME_URL ?>/assets/images/recruit/fresh_8.jpg" alt="" />
                        <div class="text">
                           <div class="figcation">オフィスツアー</div>
                           <p class="arw"><img src="<?php echo THEME_URL ?>/assets/images/recruit/arw_r_01.png" alt="" /></p>
                        </div>
                     </figure>
                  </a>
               </div>

               <div class="cntTall fl_r mr0">
                  <a href="">
                     <figure>
                        <img src="<?php echo THEME_URL ?>/assets/images/recruit/fresh_5.jpg" alt="" />
                        <div class="text">
                           <div class="figcation">社員の1週間</div>
                           <p class="arw"><img src="<?php echo THEME_URL ?>/assets/images/recruit/arw_r_01.png" alt="" /></p>
                        </div>
                     </figure>
                  </a>
               </div>

               <div class="cntSmall">
                  <a href="https://allabout-tech.hatenablog.com/" target="_blank">
                     <figure>
                        <img src="<?php echo THEME_URL ?>/assets/images/recruit/fresh_10.jpg" alt="" />
                        <div class="text">
                           <div class="figcation">テックブログ</div>
                           <p class="arw"><img src="<?php echo THEME_URL ?>/assets/images/recruit/arw_r_01.png" alt="" /></p>
                        </div>
                     </figure>
                  </a>
               </div>

               <div class="cntSmall">
                  <a href="https://corp.allabout.co.jp/recruit/fresh/index/guide.html">
                     <figure>
                        <img src="<?php echo THEME_URL ?>/assets/images/recruit/fresh_11.jpg" alt="" />
                        <div class="text">
                           <div class="figcation">All Aboutの専門家紹介</div>
                           <p class="arw"><img src="<?php echo THEME_URL ?>/assets/images/recruit/arw_r_01.png" alt="" /></p>
                        </div>
                     </figure>
                  </a>
               </div>

               <div class="cntSmall mr0">
                  <a href="http://localhost/allabout/%e3%82%b5%e3%83%bc%e3%83%93%e3%82%b9/">
                     <figure>
                        <img src="<?php echo THEME_URL ?>/assets/images/recruit/service_logo.jpg" alt="" />
                        <div class="text">
                           <div class="figcation">サービス紹介</div>
                           <p class="arw"><img src="<?php echo THEME_URL ?>/assets/images/recruit/arw_r_01.png" alt="" /></p>
                        </div>
                     </figure>
                  </a>
               </div>
            </div>

            <ul class="recruit__relateList">
               <li>
                  <a href="https://corp.allabout.co.jp/recruit/fresh/index/profile.html">
                     <figure>
                        <img src="<?php echo THEME_URL ?>/assets/images/recruit/profile.png" alt="" />
                     </figure>
                  </a>
               </li>
               <li>
                  <a href="https://allabout.snar.jp/index.aspx">
                     <figure>
                        <img src="<?php echo THEME_URL ?>/assets/images/recruit/mypage.png" alt="" />
                     </figure>
                  </a>
               </li>
               <li>
                  <a href="https://corp.allabout.co.jp/recruit/fresh/index/faq.html">
                     <figure>
                        <img src="<?php echo THEME_URL ?>/assets/images/recruit/faq.png" alt="" />
                     </figure>
                  </a>
               </li>
            </ul>
         </div>
      </div>
   </main>
   <!--▲ Main ▲-->
<?php get_footer(); ?>