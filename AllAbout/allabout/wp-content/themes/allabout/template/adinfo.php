<?php /* Template Name: Adinfo */ ?>
<?php get_header(); ?>
   <!--▼ Main ▼-->
   <main class="main bgAdinfo" id="adinfo">
      <!-- Listservice -->
      <nav class="listservice subPage">
        <?php allabout_menu('menu_serviceList'); ?>
      </nav>
      <div class="content clearfix">
         <!-- Breacrum star -->
         <?php custom_breadcrumbs(); ?>
         <!-- Content -->
         <!-- adinfo -->
         <div class="adinfo adinfo__content">
            <section class="adinfo__top">
               <h2 class="adinfo__ttl">広告商品のご案内</h2>
               <a href="http://localhost/allabout/%e5%ba%83%e5%91%8a%e5%95%86%e5%93%81%e3%81%ae%e3%81%94%e6%a1%88%e5%86%85/%e3%83%a1%e3%83%87%e3%82%a3%e3%82%a2%e7%b4%b9%e4%bb%8b/">
                  <img src="<?php echo THEME_URL ?>/assets/images/adinfo/slide_mediainfo_new-980x4201-980x420.jpg" alt="" />
               </a>
            </section>
            <section class="adinfo__bottom">
               <h3>ttl</h3>
               <ul>
                  <li><a href="http://localhost/allabout/%e5%ba%83%e5%91%8a%e5%95%86%e5%93%81%e3%81%ae%e3%81%94%e6%a1%88%e5%86%85/%e3%83%a1%e3%83%87%e3%82%a3%e3%82%a2%e7%b4%b9%e4%bb%8b/">
                     <img src="<?php echo THEME_URL ?>/assets/images/adinfo/adinfo_aatop.gif" alt="" />
                     メディア紹介
                  </a></li>
                  <li><a href="http://localhost/allabout/%e5%ba%83%e5%91%8a%e5%95%86%e5%93%81%e3%81%ae%e3%81%94%e6%a1%88%e5%86%85/%e5%ba%83%e5%91%8a%e5%95%86%e5%93%81%e4%b8%80%e8%a6%a7/">
                     <img src="<?php echo THEME_URL ?>/assets/images/adinfo/10132106433.jpg" alt="" />
                     広告商品一覧
                  </a></li>
                  <li><a href="http://localhost/allabout/%e5%ba%83%e5%91%8a%e5%95%86%e5%93%81%e3%81%ae%e3%81%94%e6%a1%88%e5%86%85/%e5%ba%83%e5%91%8a%e6%8e%b2%e8%bc%89%e5%9f%ba%e6%ba%96/">
                     <img src="<?php echo THEME_URL ?>/assets/images/adinfo/10367006031.jpg" alt="" />
                     広告掲載基準
                  </a></li>
                  <li><a href="http://localhost/allabout/%e5%ba%83%e5%91%8a%e5%95%86%e5%93%81%e3%81%ae%e3%81%94%e6%a1%88%e5%86%85/%e5%ba%83%e5%91%8a%e3%81%ab%e9%96%a2%e3%81%99%e3%82%8b%e3%81%8a%e5%95%8f%e3%81%84%e5%90%88%e3%82%8f%e3%81%9b/">
                     <img src="<?php echo THEME_URL ?>/assets/images/adinfo/10387001040.jpg" alt="" />
                     お問い合わせ
                  </a></li>
               </ul>
               <ul class="adinfo__textBannerList">
                  <li><a href="http://localhost/allabout/%e5%ba%83%e5%91%8a%e5%95%86%e5%93%81%e3%81%ae%e3%81%94%e6%a1%88%e5%86%85/%e8%b3%87%e6%96%99%e3%83%80%e3%82%a6%e3%83%b3%e3%83%ad%e3%83%bc%e3%83%89/" class="primary">資料ダウンロード</a></li>
                  <li><a href="http://localhost/allabout/%e5%ba%83%e5%91%8a%e5%95%86%e5%93%81%e3%81%ae%e3%81%94%e6%a1%88%e5%86%85/%e5%ba%83%e5%91%8a%e3%81%ab%e9%96%a2%e3%81%99%e3%82%8b%e3%81%8a%e5%95%8f%e3%81%84%e5%90%88%e3%82%8f%e3%81%9b/" class="primary">広告に関するお問い合わせ</a></li>
                  <li><a href="http://localhost/allabout/%e5%ba%83%e5%91%8a%e5%95%86%e5%93%81%e3%81%ae%e3%81%94%e6%a1%88%e5%86%85/%e5%ba%83%e5%91%8a%e6%8e%b2%e8%bc%89%e5%9f%ba%e6%ba%96/">広告掲載基準</a></li>
                  <li><a href="http://localhost/allabout/%e5%ba%83%e5%91%8a%e5%95%86%e5%93%81%e3%81%ae%e3%81%94%e6%a1%88%e5%86%85/all-about-ad-news/" target="_blank">AD News</a></li>
               </ul>
            </section>
         </div>
      </div>

      <!-- List News All -->
      <section class="adinfo__allNews">
         <div class="content">
            <h3 class="ttl">All About AD News</h3>
            <ul>
               <?php
                  $args_new = array(
                     'posts_per_page' => 8,
                     'post_status' => 'publish',
                     'cat' => 24,
                  );
                  $the_query_new = new WP_Query( $args_new );

                  if ( $the_query_new->have_posts() ) :

                  while ( $the_query_new->have_posts() ) : $the_query_new->the_post();

                  ?>
                     <li>
                        <span class="time"><?php echo get_the_date('Y年m月d日'); ?></span>
                        <a href="<?php the_permalink(); ?>" class="newsContent"><?php the_title(); ?></a>
                     </li>
                  <?php

                  endwhile;

                  endif;

                  wp_reset_postdata();

                  ?>
               </li>
            </ul>
         </div>
      </section>  
   </main>
   <!--▲ Main ▲-->
<?php get_footer(); ?>