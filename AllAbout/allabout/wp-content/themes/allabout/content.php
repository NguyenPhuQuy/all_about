<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="subTitle">
		<?php allabout_header(); ?>
	</div>
	<div class="entry-date">
		<?php allabout_date(); ?>
	</div>
	<div class="entry-content">
		<?php the_content(); ?>
	</div>
</article>